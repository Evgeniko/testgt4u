<?php
/**
 * Created by PhpStorm.
 * User: AIIST
 * Date: 21.11.2018
 * Time: 4:31
 */

namespace Controllers;


use Models\FormData;

class FormController
{
    private $model;

    public function __construct()
    {
        $this->model = new FormData();
    }

    //Показ главной страницы (формы)
    public function actionIndex()
    {
        require_once (ROOT.'/views/form.php');
        return true;
    }

    //Сохранение введённых данных в форму
    public function actionCreate()
    {
        $last_name = isset($_POST['last_name']) ? $_POST['last_name'] : NULL;
        $birthday_date = isset($_POST['birthday_date']) ? $_POST['birthday_date'] : NULL;
        $phone_number = isset($_POST['phone_number']) ? $_POST['phone_number'] : NULL;
        $email = isset($_POST['email']) ? $_POST['email'] : NULL;
        $auto_brand = isset($_POST['auto_brand']) ? $_POST['auto_brand'] : NULL;

        //Преобразование номера
        $phone_number = $this->telephoneReform($phone_number);

        //Проверка есть ли данный номер в системе
        $checkTel = $this->checkTelephoneNumber($phone_number);
        if (!$checkTel){
            echo 150;exit();
        }

        $result = $this->model->create($last_name, $birthday_date, $phone_number, $email, $auto_brand);
        echo $result;
    }

    //Показ всех сохранённых ранее данных
    public function actionShow()
    {
        $itemList = $this->model->get();
        require_once (ROOT.'/views/show.php');
    }


    //Преобразование номера перед сохранением
    private function telephoneReform($tel)
    {
        $number = preg_replace('~\D+~','',$tel);
        $length = strlen($number);

        //Ввели мобильный
        if (($length == 10) || ($length == 11)){
            if ($number[0] == 7){
                return substr_replace($number, 8, 0,1);
            }else if ($number[0] == 9){
                return '8'.$number;
            }
        }

        //Ввели городской
        /*if ($length == 7){

        }*/
        return $number;
    }


    //Проверка на наличие телефона в БД
    private function checkTelephoneNumber($tel)
    {
        return $this->model->getByPhoneNumber($tel);
    }
}