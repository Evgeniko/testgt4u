<?php
/**
 * Created by PhpStorm.
 * User: AIIST
 * Date: 20.11.2018
 * Time: 20:52
 */

namespace Components;

class DatabaseConnection
{
    public static function connect()
    {
        $params = include(ROOT.'/config/database.php');
        $db = new \PDO("mysql:host={$params['host']};dbname={$params['dbname']}",$params['user'],$params['password']);
        /*включения вывода ошибок*/ $db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        // Задаем кодировку
        $db->exec("set names utf8");
        return $db;
    }

}