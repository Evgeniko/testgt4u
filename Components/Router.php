<?php
/**
 * Created by PhpStorm.
 * User: AIIST
 * Date: 21.11.2018
 * Time: 4:29
 */

namespace Components;


class Router
{
    private $routes;
    public function __construct()
    {
        $routesPath = ROOT.'/config/routes.php';
        $this->routes = include($routesPath);
    }
    private function getURI()
    { //Возвращает строку запроса
        if(!empty($_SERVER['REQUEST_URI'])){
            return trim($_SERVER['REQUEST_URI'],'/');
        }
    }
    public function run()
    {
        //Получаем строку запроса
        $uri = $this->getURI();
        //Проверка запроса с массивом routes.php
        foreach ($this->routes as $uriPattern => $path) {
            if (preg_match("~$uriPattern~", $uri)) {
                $internalRoute = preg_replace("~$uriPattern~", $path, $uri);
                $segments = explode('/', $internalRoute);
                $controllerName = ucfirst(array_shift($segments)) . 'Controller';
                $actionName = 'action' . ucfirst(array_shift($segments));
                $parameters = $segments;
                //Подключение файлов контроллера
                try {
                    $controllerName = '\Controllers\\' . $controllerName;
                    $controllerObject = new $controllerName;
                    $result = call_user_func_array(array($controllerObject, $actionName), $parameters);
                    if ($result != null) {
                        break;
                    }
                } catch (\Error $e) {
                    http_response_code(404);
                }

            } else {
                http_response_code(404);
            }
        }
    }
}