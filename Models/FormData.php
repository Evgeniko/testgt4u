<?php
/**
 * Created by PhpStorm.
 * User: AIIST
 * Date: 20.11.2018
 * Time: 18:53
 */

namespace Models;

use Components\DatabaseConnection;

class FormData
{
    private $db;
    const SHOW_BY_DEFAULT = 150;

    public function __construct()
    {
        $this->db = DatabaseConnection::connect();
    }


    //Создание записи в БД
    public function create($last_name, $birthday_date, $phone_number, $email, $auto_brand)
    {
        try{
            $sql = "INSERT INTO data (last_name, birthday_date, telephone_number, email, auto_brand) VALUES (:last_name, :birthday_date, :telephone_number, :email, :auto_brand)";
            $query = $this->db->prepare($sql);
            $result = $query->execute([':last_name' => $last_name, ':birthday_date' => $birthday_date, ':telephone_number' => $phone_number, ':email' => $email, ':auto_brand' => $auto_brand]);

            if ($result){
                return 200;
            }else{
                return 400;
            }
        }catch (\PDOException $e){
            return 400;
        }
    }

    //Получение всех введёных ранее данных
    public function get($count = self::SHOW_BY_DEFAULT,$page = 1)
    {
        $list = [];
        $count = intval($count);
        $page = intval($page);
        $offset = ($page - 1) * self::SHOW_BY_DEFAULT;
        $result = $this->db->query("SELECT * FROM data ORDER BY id ASC LIMIT $count OFFSET $offset");
        foreach($result as $row){
            $list[] = array(
                'id' => $row['id'],
                'last_name' => $row['last_name'],
                'birthday_date' => $row['birthday_date'],
                'phone_number' => $row['telephone_number'],
                'email' => $row['email'],
                'auto_brand' => $row['auto_brand']
            );
        }
        return $list;
    }

    public function update()
    {

    }

    public function delete($id)
    {

    }

    //Получение данных по номеру телефона
    public function getByPhoneNumber($tel)
    {
        $data = $this->db->prepare('SELECT * FROM data WHERE telephone_number = :tel');
        $data->execute([':tel' => $tel]);
        $result = $data->fetchAll();
        //Найдено совпадение
        if (count($result)){
            return false;
        }
        return true;
    }
}