<table border="1">
    <caption>Таблица результатов</caption>
    <tr>
        <th>id</th>
        <th>Фамилия</th>
        <th>Дата рождения</th>
        <th>Телефон</th>
        <th>Email</th>
        <th>Марка авто</th>
    </tr>
    <?php if (count($itemList)) : ?>
    <?php foreach ($itemList as $item) : ?>
        <tr>
            <td><?= $item['id']?></td>
            <td><?= $item['last_name']?></td>
            <td><?= $item['birthday_date']?></td>
            <td><?= $item['phone_number']?></td>
            <td><?= $item['email']?></td>
            <td><?= $item['auto_brand']?></td>
        </tr>
    <?php endforeach;?>
    <?php endif; ?>