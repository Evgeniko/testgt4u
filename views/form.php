<!DOCTYPE HTML>
<html>
<head>
    <script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
</head>
<body>
<div class="form-container">
    <form name="form" method="POST" action="/form/create">
        <div>
            <label>Фамилия:<input name="last_name"></label>
        </div>
        <div>
            <label>Дата рождения:<input name="birthday_date"></label>
        </div>
        <div>
            <label>Номер телефона:<input name="phone_number" required="required"></label>
        </div>
        <div>
            <label>E-mail:<input name="email" type="email"></label>
        </div>
        <div>
            <label>Марка авто:</label>
            <select name="auto_brand">
                <option selected="selected" value="">Марки авто</option>
                <option value="BMW">BMW</option>
                <option value="AUDI">AUDI</option>
                <option value="VAZ">VAZ</option>
                <option value="FORD">FORD</option>
                <option value="VOLVO">VOLVO</option>
            </select>
        </div>
        <input type="submit" value="Отправить">
        <div class="button">
            <a class="" href="/form/show">Просмотр Добавленных данных</a>
        </div>
    </form>
</div>
<script src="/resources/main.js"></script>
</body>
</html>
<style>
    .form-container{
        text-align: center;
    }
    .button{
        margin-top: 20px;
        padding: 10px;
        background-color: cadetblue;
        color: white;
    }
</style>